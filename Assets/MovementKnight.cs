﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moument : MonoBehaviour {

    [SerializeField]
    float MovementSpeed;

    int Lives = 3;

    public int GetLives()
    {
        return Lives;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        //Debug.Log("h:" + h + " - v:" + v );
        if (h != 0)
        {
            transform.Translate(Vector3.right * MovementSpeed * h);

        }
        else
        {
            transform.Translate(Vector3.forward * MovementSpeed * v);

        }

    }
}
